# -*- coding: utf-8 -*-

from odoo import models, fields, api

class nganhnghe(models.Model):
    _name = 'donhang.nganhnghe'
    _rec_name = 'ten_nn'

    ten_nn = fields.Char(string='Tên ngành nghề')

class loaicv(models.Model):
    _name = 'donhang.loaicv'
    _rec_name = 'loai_cv'

    loai_cv = fields.Char(string='Loại Công việc')

class congviec(models.Model):
    _name = 'donhang.congviec'
    _rec_name = 'c_v'

    c_v = fields.Char(string='Công việc')

class donhang(models.Model):
    _name = 'donhang.donhang'
    _rec_name = 'ten_don_hang'

    ma_don_hang = fields.Integer(string='Mã Đơn Hàng',required=True)
    ten_don_hang = fields.Char(string='Tên Đơn Hàng',required=True)
    congty_pahicu = fields.Many2one(comodel_name='congty.congty', string='Công Ty phải cử')
    # congty_pahicu = fields.Char(string='Công Ty phải cử',required=True)
    xi_nghiep = fields.Many2one(comodel_name='xinghiep.xinghiep', string='Xí Nghiệp' )
    # xi_nghiep = fields.Char(string='Xí Nghiệp',required=True)
    nganh_nghe = fields.Many2one(comodel_name='donhang.nganhnghe', string='Ngành Nghề')
    # nganh_nghe = fields.Selection(string='Ngành Nghề',selection=[('1', '有限責任会社'), ('2', 'コングロマリット')])
    loai_cv = fields.Many2one(comodel_name='donhang.loaicv', string='Loại CV')
    # loai_cv = fields.Selection(string='Loại CV',selection=[('1', 'Làm đúc'), ('2', 'LÀm óin')])
    cong_viec = fields.Many2one(comodel_name='donhang.congviec', string='Công việc')
    # cong_viec = fields.Selection(string='Công việc',selection=[('1', 'Làm ....'), ('2', 'LÀm ....')])
    so_thuc_tap_sinh_nam = fields.Integer(string='Số thực tập sinh nam',required=True)
    so_thuc_tap_sinh_nu = fields.Integer(string='Số thực tập sinh nữ',required=True)
    tong_thuc_tap_sinh = fields.Integer(string='Tổng thực tập sinh', compute='_tong_thuc_tap_sinh')
    thoi_gian_nhap_quoc = fields.Date(string='Thời gian nhập quốc',required=True)
    @api.one
    @api.depends('so_thuc_tap_sinh_nam','so_thuc_tap_sinh_nu')
    def _tong_thuc_tap_sinh(self):
        if self.so_thuc_tap_sinh_nam and self.so_thuc_tap_sinh_nu:
            self.tong_thuc_tap_sinh = self.so_thuc_tap_sinh_nam + self.so_thuc_tap_sinh_nu
        elif self.so_thuc_tap_sinh_nam:
            self.tong_thuc_tap_sinh = self.so_thuc_tap_sinh_nam
        elif self.so_thuc_tap_sinh_nu:
            self.tong_thuc_tap_sinh = self.so_thuc_tap_sinh_nu
        else:
            self.tong_thuc_tap_sinh = 0



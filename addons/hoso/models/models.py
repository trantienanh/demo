# -*- coding: utf-8 -*-

from odoo import models, fields, api

class hoso(models.Model):
    _name = 'hoso.hoso'

    ngay_nhap_quoc = fields.Char(string='Ngày Nhập Quốc')
    don_hang = fields.Char(string='Đơn Hàng')
    xi_nghiep = fields.Many2one(comodel_name='xinghiep.xinghiep', string='Xí Nghiệp')
    giai_doan = fields.Char(string='Giai Đoạn')
    ngay_lapvb = fields.Char(string='Ngày lập văn bản')
    chi_nhanh = fields.Many2one(comodel_name='baocao.xinghiep1', string='Chi Nhánh')
    danh_sach = fields.Many2many(comodel_name='hoso.ttsinh', string='Danh Sách Thực Tập Sinh')

class ttsinh(models.Model):
    _name = 'hoso.ttsinh'

    ma_tts = fields.Integer(string='Mã thực tập sinh')
    ten = fields.Char(string='Tên(Latin)')
    gioi_tinh = fields.Char(string='Giới Tính')
    quoc_tich = fields.Char(string='Quốc Tịch')
    ngay_sinh = fields.Date(string='Ngày Sinh')
    dia_chi = fields.Char(string='Địa chỉ hiện tại')
    thong_tinkhac = fields.Char(string='Thông tin khác')

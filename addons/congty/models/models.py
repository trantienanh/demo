# -*- coding: utf-8 -*-

from odoo import models, fields, api

class congty(models.Model):
    _name = 'congty.congty'
    _rec_name = 'ten_cong_ty'
    ten_cong_ty = fields.Char(string='Tên công ty (Tiếng Anh)', required=True)
    ten_cong_ty1 = fields.Char(string='Tên công ty (Tiếng Việt)', required=True)
    dc_cong_ty = fields.Char(string='Địa chỉ công ty (Tiếng Anh)', required=True)
    ten_gd_cong_ty = fields.Char(string='Tên giám đóc công ty (Tiếng Việt)', required=True)
    chuc_danh = fields.Char(string='Chức danh thư ký PC (Tiếng Nhật)', required=True)
    so_dt = fields.Integer(string='Số điện thoại', required=True)
    so_fax = fields.Integer(string='Số Fax')
    ngay_thanh_lap = fields.Date(string='Ngày thành lập công ty')


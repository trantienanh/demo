# -*- coding: utf-8 -*-

from odoo import models, fields, api


class baocao(models.Model):
    _name = 'baocao.baocao'
    _rec_name = 'don_hang'

    don_hang = fields.Many2one(comodel_name='donhang.donhang', string='Đơn Hàng')
    giai_doan = fields.Char(string='Giai Đoạn')
    nganh_nghe = fields.Many2one(comodel_name='donhang.nganhnghe', string='Ngành Nghề')
    cong_viec = fields.Many2one(comodel_name='donhang.congviec', string='Công Việc')
    xi_nghiep = fields.Many2one(comodel_name='xinghiep.xinghiep', string='Xí Nghiệp')
    loai_cv = fields.Many2one(comodel_name='donhang.loaicv', string='Loại Công Việc')
    ma_cv = fields.Char(string='Mã Công Việc')
    chi_nhanh = fields.One2many(comodel_name='baocao.xinghiep1', inverse_name="chi_nhanh", string='Chi Nhánh')
    bc_xn = fields.Many2many(comodel_name='baocao.xinghiep1', string='Danh sách')


class xinghiep1(models.Model):
    _name = 'baocao.xinghiep1'
    _rec_name = 'ten_chinhanh'

    chi_nhanh = fields.Many2one(comodel_name='baocao.baocao', string='Chi nhánh')
    ten_chinhanh = fields.Char(string='Tên chi nhánh')
    dichi_chinhanh = fields.Char(string='Địa chỉ')

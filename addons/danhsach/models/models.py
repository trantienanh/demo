# -*- coding: utf-8 -*-

from odoo import models, fields, api

class danhsach(models.Model):
    _name = 'danhsach.danhsach'
    _rec_name = 'ten_day_du'

    ten_viet_tat = fields.Char(string='Tên viết tắt - chữ Romaji')
    ten_day_du = fields.Char(string='Tên đầy đủ - chữ Hán')
    dia_chi = fields.Char(string='Địa chỉ - chữ Nhật')
    dia_chi1 = fields.Char(string='Địa chỉ - chữ Romaji')
    ma_bd = fields.Integer(string='Mã bưu điện bằng số')
    so_dt = fields.Integer(string='Số điện thoại')
    ten_day_du_anh = fields.Char(string='Tên đầy đủ - tiếng Anh')
    so_giay_phep = fields.Integer(string='Số giấy phép')
    so_Fax = fields.Integer(string='Số Fax (Nếu Có)')
    cv_dd = fields.Char(string='Chức vụ người đại diện(ký trong hợp đồng)-tiếng Việt')
    cvc_dd = fields.Char(string='Chức vụ của người đại diện(ký trong hợp đồng)-tiếng Hán')
    ten_nguoi_dd = fields.Char(string='Tên người đại diện - chữ Romaji')
    ten_nguoi_dd1 = fields.Char(string='Tên người đại diện - chữ Hán')
    ngay_ky = fields.Datetime(string='Ngày kí hiệp định giữa Nghiệp đoàn với pháp nhân')
    phi_dd = fields.Integer(string='Phí ủy thác đào tạo (Yên)')
    tro_cap_dd = fields.Integer(string='Trợ cấp đào tạo tháng đầu (Yên)')
    ghi_chu_nhat = fields.Char(string='Ghi chú tiếng Nhật')
    ghi_chu_viet = fields.Char(string='Ghi chú tiếng Việt')
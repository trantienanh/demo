# -*- coding: utf-8 -*-

from odoo import models, fields, api

class xinghiep(models.Model):
    _name = 'xinghiep.xinghiep'
    _rec_name = 'ten_xi_nghiep'

    ten_xi_nghiep = fields.Char(string='Tên xí nghiệp - Tiếng Hán', required=True)
    dia_chi = fields.Char(string='Địa chỉ làm việc - Tiếng Hán (Lấy từ bản hợp đồng lương)')
    so_dt = fields.Integer(string='Số điện thoại')
    ten_nguoi_dd = fields.Char(string='Tên người đại diện - Tiếng Nhật')
    ten_xi_nghiep1 = fields.Char(string='Tên xí nghiệp - Tiếng Romaji', required=True)
    dia_chi1 = fields.Char(string='Địa chỉ làm việc - Tiếng Romaji (Tự phiên âm, kiểm tra với khách hàng và PTTT trước khi điền vào HS)')
    so_fax = fields.Integer(string='Số Fax')
    ten_nguoi_anh = fields.Char(string='Tên người đại diện - Tiếng Anh')


